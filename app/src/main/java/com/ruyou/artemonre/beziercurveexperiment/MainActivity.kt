package com.ruyou.artemonre.beziercurveexperiment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val view = BezierLineView(this)
        view.userLevel = 2
        view.levels = listOf(
            LevelItem(5, "Гуру","Скидка на всё 50%"),
            LevelItem(4, "Мастер", "Скидка на всё 40%"),
            LevelItem(3, "Эксперт", "Скидка на всё 30%"),
            LevelItem(2, "Ученик", "Скидка на всё 20%"),
            LevelItem(1, "Новичок", "Скидка на всё 10%")
        )

        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            (view.levels.size + 1) * 160
        )
        view.setPadding(32, 0, 32, 0)

        findViewById<LinearLayout>(R.id.main_layout).addView(view, params)

//        val recyclerView: RecyclerView = findViewById(R.id.view)

//        recyclerView.layoutManager = LinearLayoutManager(this)
//        recyclerView.adapter = BezierLineAdapter(BezierLineAdapter.OnClickListener {
//
//        })

//        (recyclerView.adapter as BezierLineAdapter).data = listOf(5, 4, 3, 2, 1, 0)
    }
}