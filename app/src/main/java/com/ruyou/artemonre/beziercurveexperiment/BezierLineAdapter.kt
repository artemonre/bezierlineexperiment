package com.ruyou.artemonre.beziercurveexperiment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ruyou.artemonre.beziercurveexperiment.databinding.ItemBezierLineBinding

class BezierLineAdapter(
    private val onClickListener: OnClickListener
) : RecyclerView.Adapter<BezierLineAdapter.ViewHolder>() {

    var data = listOf<Int>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class ViewHolder private constructor(
        val binding: ItemBezierLineBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            item: Int,
            onClickListener: OnClickListener
        ) {
        }

        companion object {
            fun from(
                parent: ViewGroup,
                fromScreenStart: Boolean,
                itemOrder: BezierLineView.ItemOrder,
                item: Int
            ): ViewHolder {
                val binding = ItemBezierLineBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )

                binding.lineView.userLevel = 3
                binding.lineView.level = item
                binding.lineView.itemOrder = itemOrder
                binding.lineView.fromScreenStart = fromScreenStart

                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder.from(
            parent,
            viewType % 2 != 0,
            when (viewType) {
                0 -> BezierLineView.ItemOrder.FIRST
                data.size - 1 -> BezierLineView.ItemOrder.LAST
                else -> BezierLineView.ItemOrder.COMMON
            },
            data[viewType]
        )

    override fun onBindViewHolder(holder: BezierLineAdapter.ViewHolder, position: Int) {
        val item = data[position]

        holder.bind(item, onClickListener)
    }

    override fun getItemViewType(position: Int) = position

    override fun getItemCount() = data.size

    class OnClickListener(val clickListener: (item: Int) -> Unit) {
        fun onClick(item: Int) = clickListener(item)
    }
}