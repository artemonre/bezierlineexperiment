package com.ruyou.artemonre.beziercurveexperiment

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat


class ExperimentGradientView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initStartAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet , defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initStartAttributes(attrs)
    }

    var levels: List<LevelItem> = listOf()

    private var paintBackgroundColor: Int = 0
    private var paintBackground: Paint = Paint()
    private var paintProgressColor: Int = 0
    private var paintProgress: Paint = Paint()
    private var paintCircle: Paint = Paint()
    private var paintRoundSelect: Paint = Paint()
    private var paintTextPrimary: Paint = Paint()
    private var paintTextSecondary: Paint = Paint()
    private var paintTextCircle: Paint = Paint()
    private var rect: RectF? = null
    private var pathBackground: Path = Path()
    private var pathProgressWithGradient: Path = Path()
    private var pathProgress: Path = Path()
    private var strokeWidth: Float = 200f
    private var isReverse = false

    var fromScreenStart = false

    enum class ItemOrder { FIRST, COMMON, LAST }
    var itemOrder:ItemOrder = ItemOrder.COMMON

    var height = 0f
    var stepHeight = 160
    var stepHeightHalf = stepHeight / 2
    var width = 0f

    val startPoint = Point((strokeWidth / 2).toInt(), 0 - stepHeightHalf)

    private val straightLineEndPoint: Point = Point()
    private val firstAnchorPoint: Point = Point()
    private val secondAnchorPoint: Point = Point()
    private val lineEndPoint: Point = Point()
    private val circleCenterPoint: Point = Point()
    private val textPrimaryStartPoint: Point = Point()
    private val textSecondaryStartPoint: Point = Point()
    private val textCircleStartPoint: Point = Point()
    private val progressStartPoint: Point = Point()

    var bowEdgeX = 0
    var bowCurveX = 0

    var userCurrentLevel = 0
    var level = 1

    val allCirclesCenters = ArrayList<Point>()

    private fun initStartAttributes(attrs: AttributeSet) {
    }

    private fun setPaintLine(paint: Paint, color: Int) {
        paint.isAntiAlias = true
        paint.style = Paint.Style.STROKE
        paint.strokeCap = Paint.Cap.ROUND
        paint.strokeWidth = strokeWidth

        paint.color = color
    }

    private fun setPaintText(paint: Paint, textSize: Float, isBold: Boolean, color: Int) {
        paint.isAntiAlias = true
        paint.style = Paint.Style.STROKE
        paint.textSize = textSize
        paint.isFakeBoldText = isBold
        paint.color = color
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        width = (measuredWidth - (paddingLeft + paddingRight)).toFloat()
        strokeWidth = width
        height = measuredHeight.toFloat()
//        stepHeight = height.toInt()
//        stepHeightHalf = stepHeight / 2
        startPoint.y = 0 - stepHeightHalf

        if (rect == null) {
            rect = RectF(
                paddingLeft.toFloat(),
                0f,
                    width + paddingRight,
                    height
            )

            paintBackgroundColor = ContextCompat.getColor(context, R.color.grey)
            paintProgressColor = ContextCompat.getColor(context, R.color.purple_200)

            setPaintLine(paintBackground, paintBackgroundColor)
            setPaintLine(paintProgress, paintProgressColor)
            setPaintLine(paintCircle, ContextCompat.getColor(context, R.color.grey))
            setPaintLine(paintRoundSelect, ContextCompat.getColor(context, R.color.white))
            setPaintText(paintTextPrimary, 26f, true, ContextCompat.getColor(context, R.color.grey))
            setPaintText(paintTextSecondary, 22f, false, ContextCompat.getColor(context, R.color.grey))
            setPaintText(paintTextCircle, 26f, true, ContextCompat.getColor(context, R.color.white))
        }
    }

    var testPoint1 = Point()
    var testPoint2 = Point()
    var testPoint3 = Point()

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val startY: Float = (height * 0.5).toFloat()

        Log.i("mainLog", "height = $height, startY = $startY")

        paintProgress.shader = LinearGradient(
            (width / 2) + paddingLeft,
                startY - 100,
            (width / 2) + paddingLeft,
                startY + 100,
            ContextCompat.getColor(context, R.color.grey),
            ContextCompat.getColor(context, R.color.purple_200),
            Shader.TileMode.CLAMP
        )

        canvas.drawLine(width / 2, 0f, width / 2, height, paintProgress)
    }

    private fun drawOneStep(
            straightLineEndPoint: Point,
            firstAnchorPoint: Point,
            secondAnchorPoint: Point,
            lineEndPoint: Point,
            path: Path
    ) {
        path.lineTo(straightLineEndPoint.x.toFloat(), straightLineEndPoint.y.toFloat())
        path.cubicTo(
                firstAnchorPoint.x.toFloat(),
                firstAnchorPoint.y.toFloat(),
                secondAnchorPoint.x.toFloat(),
                secondAnchorPoint.y.toFloat(),
                lineEndPoint.x.toFloat(),
                lineEndPoint.y.toFloat()
        )
    }

    private fun drawOneStepProgress(
            straightLineEndPoint: Point,
            firstAnchorPoint: Point,
            secondAnchorPoint: Point,
            lineEndPoint: Point,
            path: Path
    ) {
        path.lineTo(straightLineEndPoint.x.toFloat(), straightLineEndPoint.y.toFloat())
        path.cubicTo(
                firstAnchorPoint.x.toFloat(),
                firstAnchorPoint.y.toFloat(),
                secondAnchorPoint.x.toFloat(),
                secondAnchorPoint.y.toFloat(),
                lineEndPoint.x.toFloat(),
                lineEndPoint.y.toFloat()
        )
    }

    companion object {
        private const val START_ANGLE_POINT_DEFAULT = 270f

        private const val ATTR_NAME_BACK_LINE_COLOR = "backLineColor"
        private const val ATTR_NAME_PROGRESS_LINE_COLOR = "progressLineColor"
        private const val ATTR_NAME_LINE_WIDTH = "lineWidth"
        private const val ATTR_NAME_START_ANGLE = "startAngle"
        private const val ATTR_NAME_IS_REVERSE = "isReverse"
    }
}