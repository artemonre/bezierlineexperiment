package com.ruyou.artemonre.beziercurveexperiment

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.round


class BezierLineView : View {

    var userLevel = 0
    var level = 1

    var fromScreenStart = false

    var height = 0f
    var stepHeight = 160
    var stepHeightHalf = stepHeight / 2
    var width = 0f

    var levels: List<LevelItem> = listOf()
    private val emptyZeroLevel = LevelItem(0, "", "")
    private val emptyLastLevel = LevelItem(0, "", "")
    private lateinit var currentLevel: LevelItem
    private var maxLevelItem = 0

    private var paintBackground: Paint = Paint()
    private var paintCircle: Paint = Paint()
    private var paintRoundSelect: Paint = Paint()
    private var paintTextPrimary: Paint = Paint()
    private var paintTextSecondary: Paint = Paint()
    private var paintTextCircle: Paint = Paint()

    private var rect: RectF? = null
    private var pathBackground: Path = Path()

    private var strokeWidth: Float = 24f
    private var isReverse = false

    enum class ItemOrder { FIRST, COMMON, LAST }

    var itemOrder: ItemOrder = ItemOrder.COMMON

    private val startPoint = Point((strokeWidth / 2).toInt(), 0 - stepHeightHalf)

    private val straightLineEndPoint: Point = Point()
    private val firstAnchorPoint: Point = Point()
    private val secondAnchorPoint: Point = Point()
    private val lineEndPoint: Point = Point()
    private val circleCenterPoint: Point = Point()
    private val textPrimaryStartPoint: Point = Point()
    private val textSecondaryStartPoint: Point = Point()
    private val textCircleStartPoint: Point = Point()
    private val progressStartPoint: Point = Point()

    private var startBowEdge = 0
    private var endBowEdge = 0
    private var bowEdgeX = 0
    private var bowCurveX = 0
    private var anchorPointsDiameter = 0f
    private var circleRadius = 0f

    private var topGradientPoint = -10
    private var bottomGradientPoint = topGradientPoint + GRADIENT_SIZE

    private val allCirclesCenters = ArrayList<Point>()

    constructor(context: Context) : super(context) {
        initPaints()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initStartAttributes(attrs)
        initPaints()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initStartAttributes(attrs)
        initPaints()
    }

    private fun initStartAttributes(attrs: AttributeSet) {
    }

    private fun initPaints() {
        setPaintLine(paintBackground, ContextCompat.getColor(context, R.color.grey))
        setPaintLine(paintCircle, ContextCompat.getColor(context, R.color.grey))
        setPaintLine(paintRoundSelect, ContextCompat.getColor(context, R.color.white))
        paintRoundSelect.strokeWidth = 4f
        setPaintText(paintTextPrimary, 26f, true, ContextCompat.getColor(context, R.color.grey))
        setPaintText(
            paintTextSecondary,
            22f,
            false,
            ContextCompat.getColor(context, R.color.grey)
        )
        setPaintText(paintTextCircle, 26f, true, ContextCompat.getColor(context, R.color.white))
    }

    private fun setPaintLine(paint: Paint, color: Int) {
        paint.isAntiAlias = true
        paint.style = Paint.Style.STROKE
        paint.strokeCap = Paint.Cap.ROUND
        paint.strokeWidth = strokeWidth

        paint.color = color
    }

    private fun setPaintText(paint: Paint, textSize: Float, isBold: Boolean, color: Int) {
        paint.isAntiAlias = true
        paint.style = Paint.Style.STROKE
        paint.textSize = textSize
        paint.isFakeBoldText = isBold
        paint.color = color
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        width = (measuredWidth - (paddingLeft + paddingRight)).toFloat()
        height = measuredHeight.toFloat()

        startBowEdge = stepHeightHalf + (strokeWidth / 2).toInt() + paddingLeft
        endBowEdge = (width - (stepHeightHalf) - strokeWidth / 2).toInt() + paddingRight

        anchorPointsDiameter = (stepHeight / 1.5).toFloat()
        circleRadius = round(anchorPointsDiameter / 2.2).toFloat()
        paintCircle.strokeWidth = circleRadius

        if (rect == null) {
            rect = RectF(
                paddingLeft.toFloat(),
                0f,
                width + paddingRight,
                height
            )
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (rect != null) {
            pathBackground.moveTo(startPoint.x.toFloat(), startPoint.y.toFloat())

            maxLevelItem = levels.size + 1
            for (i in 0..maxLevelItem) {
                calculateAllElements(canvas, i)
            }

            setTouchListener(circleRadius)
        }
    }

    private fun calculateAllElements(canvas: Canvas, index: Int) {
        currentLevel = getCurrentLevel(index)

        setXCoordinates(index, anchorPointsDiameter, circleRadius)
        setYCoordinates(index, circleRadius)

        drawOneStep(pathBackground)

        if (userLevel + 1 == currentLevel.levelNumber) {
            setProgressPoint()
        }

        setGradientCoordinates()

        if (index != 0 && index != maxLevelItem) {
            drawTextesAndCircles(canvas)
        }

        paintBackground.shader = getGradient()
        canvas.drawPath(pathBackground, paintBackground)
    }

    private fun getCurrentLevel(index: Int) = when {
        index - 1 in levels.indices -> levels[index - 1]
        index == 0 -> emptyLastLevel.also { it.levelNumber = levels[index].levelNumber + 1 }
        else -> emptyZeroLevel
    }

    private fun setXCoordinates(index: Int, anchorPointsDiameter: Float, circleRadius: Float) {
        if ((!fromScreenStart && index % 2 == 0) || (fromScreenStart && index % 2 != 0)) {
            setXForStartSide(anchorPointsDiameter, circleRadius)
        } else {
            setXForEndSide(anchorPointsDiameter, circleRadius)
        }

        straightLineEndPoint.x = bowEdgeX
        firstAnchorPoint.x = bowCurveX
        secondAnchorPoint.x = bowCurveX
        lineEndPoint.x = bowEdgeX
        circleCenterPoint.x = bowEdgeX

        textCircleStartPoint.x =
            (circleCenterPoint.x - ((paintTextCircle.measureText("${currentLevel.levelNumber}")) / 2)).toInt()
    }

    private fun setXForStartSide(anchorPointsDiameter: Float, circleRadius: Float) {
        bowEdgeX = endBowEdge
        bowCurveX = (endBowEdge + anchorPointsDiameter).toInt()
        textPrimaryStartPoint.x =
            (
                    bowEdgeX - circleRadius - strokeWidth - (
                            paintTextPrimary.measureText(currentLevel.levelTitle)
                            )
                    ).toInt()
        textSecondaryStartPoint.x =
            (
                    bowEdgeX - circleRadius - strokeWidth - (
                            paintTextSecondary.measureText(currentLevel.levelDescription)
                            )
                    ).toInt()
    }

    private fun setXForEndSide(anchorPointsDiameter: Float, circleRadius: Float) {
        bowEdgeX = startBowEdge
        bowCurveX = (startBowEdge - anchorPointsDiameter).toInt()
        textPrimaryStartPoint.x = (startBowEdge + circleRadius + strokeWidth).toInt()
        textSecondaryStartPoint.x = (startBowEdge + circleRadius + strokeWidth).toInt()
    }

    private fun setYCoordinates(index: Int, circleRadius: Float) {
        straightLineEndPoint.y = startPoint.y + (stepHeight * index)
        firstAnchorPoint.y = startPoint.y + (stepHeight * index)
        secondAnchorPoint.y = startPoint.y + (stepHeight * (index + 1))
        lineEndPoint.y = startPoint.y + (stepHeight * (index + 1))
        circleCenterPoint.y = startPoint.y + stepHeightHalf + (stepHeight * index)
        textPrimaryStartPoint.y =
            ((circleCenterPoint.y - circleRadius) - paintTextPrimary.fontMetrics.top + 12).toInt()
        textSecondaryStartPoint.y =
            (textPrimaryStartPoint.y - paintTextSecondary.fontMetrics.top + 8).toInt()

        val middle =
            abs((paintTextCircle.fontMetrics.descent - ((paintTextCircle.fontMetrics.descent - paintTextCircle.fontMetrics.ascent) / 2)).toInt())

        textCircleStartPoint.y = circleCenterPoint.y + middle
    }

    private fun setProgressPoint() {
        progressStartPoint.x = lineEndPoint.x
        progressStartPoint.y = lineEndPoint.y
    }

    private fun drawTextesAndCircles(canvas: Canvas) {
        if (userLevel == currentLevel.levelNumber) {
            paintCircle.shader = getGradient()
        }

        drawCircle(canvas, circleRadius - paintCircle.strokeWidth / 2, paintCircle)
        allCirclesCenters.add(getPoint(circleCenterPoint.x, circleCenterPoint.y))

        if (userLevel == currentLevel.levelNumber) {
            drawCircle(
                canvas,
                circleRadius - 2 - paintRoundSelect.strokeWidth / 2,
                paintRoundSelect
            )
        }

        if (userLevel == currentLevel.levelNumber) {
            paintTextPrimary.shader = getGradient()
            paintTextSecondary.shader = getGradient()
        }

        drawLevelTextes(canvas)
    }

    private fun drawLevelTextes(canvas: Canvas) {
        canvas.drawText(
            currentLevel.levelTitle,
            textPrimaryStartPoint.x.toFloat(),
            textPrimaryStartPoint.y.toFloat(),
            paintTextPrimary
        )

        canvas.drawText(
            currentLevel.levelDescription,
            textSecondaryStartPoint.x.toFloat(),
            textSecondaryStartPoint.y.toFloat(),
            paintTextSecondary
        )

        canvas.drawText(
            "${currentLevel.levelNumber}",
            textCircleStartPoint.x.toFloat(),
            textCircleStartPoint.y.toFloat(),
            paintTextCircle
        )
    }

    private fun setGradientCoordinates() {
        topGradientPoint = -10
        bottomGradientPoint = topGradientPoint + GRADIENT_SIZE
    }

    private fun drawCircle(canvas: Canvas, circleRadius: Float, paint: Paint) {
        canvas.drawCircle(
            circleCenterPoint.x.toFloat(),
            circleCenterPoint.y.toFloat(),
            circleRadius,
            paint
        )
    }

    private fun getPoint(x: Int, y: Int) = Point(x, y)

    private fun setTouchListener(circleRadius: Float) {
        setOnTouchListener { _, event ->
            for (circleCenter in allCirclesCenters) {
                if (
                    ((event.x - circleCenter.x).toDouble()).pow(2.0) +
                    ((event.y - circleCenter.y).toDouble()).pow(2.0) <
                    circleRadius.toDouble().pow(2.0)
                ) {
                    Log.i("mainLog", "circle is touched")
                }
            }

            true
        }
    }

    private fun getGradient() = LinearGradient(
        (width / 2) + paddingLeft,
        (progressStartPoint.y + topGradientPoint).toFloat(),
        (width / 2) + paddingLeft,
        (progressStartPoint.y + bottomGradientPoint).toFloat(),
        ContextCompat.getColor(context, R.color.grey),
        ContextCompat.getColor(context, R.color.pink),
        Shader.TileMode.CLAMP
    )

    private fun drawOneStep(path: Path) {
        path.lineTo(straightLineEndPoint.x.toFloat(), straightLineEndPoint.y.toFloat())
        path.cubicTo(
            firstAnchorPoint.x.toFloat(),
            firstAnchorPoint.y.toFloat(),
            secondAnchorPoint.x.toFloat(),
            secondAnchorPoint.y.toFloat(),
            lineEndPoint.x.toFloat(),
            lineEndPoint.y.toFloat()
        )
    }

    companion object {
        private const val START_ANGLE_POINT_DEFAULT = 270f

        private const val ATTR_NAME_BACK_LINE_COLOR = "backLineColor"
        private const val ATTR_NAME_PROGRESS_LINE_COLOR = "progressLineColor"
        private const val ATTR_NAME_LINE_WIDTH = "lineWidth"
        private const val ATTR_NAME_START_ANGLE = "startAngle"
        private const val ATTR_NAME_IS_REVERSE = "isReverse"

        private const val GRADIENT_SIZE = 175
    }
}

data class LevelItem(
    var levelNumber: Int,
    val levelTitle: String,
    val levelDescription: String
)