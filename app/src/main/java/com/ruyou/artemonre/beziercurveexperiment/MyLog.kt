package com.ruyou.artemonre.beziercurveexperiment

import android.util.Log

const val MAIN_LOG = "mainLog"

object MyLog {

    fun d(text: String, where: Any? = null) {
        if(where != null)
            Log.d(where.javaClass.simpleName, text)
        else
            Log.d(MAIN_LOG, text)
    }

    fun d(text: String, exception: Throwable, where: Any? = null) {
        if(where != null)
            Log.d(where.javaClass.simpleName, text, exception)
        else
            Log.d(MAIN_LOG, text, exception)
    }

    fun i(text: String, where: Any? = null) {
        if(where != null)
            Log.i(where.javaClass.simpleName, text)
        else
            Log.i(MAIN_LOG, text)
    }

    fun i(text: String, exception: Throwable, where: Any? = null) {
        if(where != null)
            Log.i(where.javaClass.simpleName, text, exception)
        else
            Log.i(MAIN_LOG, text, exception)
    }

    fun e(text: String, where: Any? = null) {
        if(where != null)
            Log.e(where.javaClass.simpleName, text)
        else
            Log.e(MAIN_LOG, text)
    }

    fun e(text: String, exception: Throwable, where: Any? = null) {
        if(where != null)
            Log.e(where.javaClass.simpleName, text, exception)
        else
            Log.e(MAIN_LOG, text, exception)
    }
}